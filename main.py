

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager

from controllers.notes_manager import NotesManager
from views.transitions import NoteTransition


class Manager(ScreenManager):
    notes_manager = NotesManager()

    def __init__(self, **kwargs):
        super().__init__(transition=NoteTransition(), **kwargs)
        self.keyboard = Window.request_keyboard(self._keyboard_closed, self)

    def on_current(self, inst, current):
        for screen in self.screens:
            if screen is self.get_screen(current):
                continue
            self.keyboard.unbind(on_key_down=screen._on_keyboard_down)
        super().on_current(inst, current)

    def _keyboard_closed(self):
        return  # For some reason this is called right after creation
        self.keyboard.unbind(on_key_down=self._on_keyboard_down)
        self.keyboard = None


class BrainDrain(App):
    manager = Manager()

    def build(self):
        return self.manager


if __name__ == '__main__':
    from kivy.config import Config
    from views.note_screen import NoteScreen
    from views.search_screen import SearchScreen
    from views.settings_screen import SettingsScreen

    Config.set('kivy', 'exit_on_escape', '0')
    bd = BrainDrain()
    bd.manager.add_widget(SearchScreen(name='search'))
    bd.manager.add_widget(NoteScreen(name='note', previous='search'))
    bd.manager.add_widget(SettingsScreen(name='settings', previous='search'))

    bd.run()
