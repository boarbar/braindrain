

import os
import re

import yaml
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout


Builder.load_file('views/main.kv')


class Main(BoxLayout):
    def __init__(self, app, default_file=None):
        super().__init__()
        self.app = app
        self.preferences = {'search_paths': ['data']}
        self.raw_data = {}
        self.last_search_text = ''
        self.current_form = None

        Window.bind(on_key_down=self.on_key_down)
        self.search_bar.entry.bind(text=self.on_input)
        self.search_bar.results.bind(
            on_select=lambda w, path: self.open_form
        )

        self.reload_raw_data()
        self.reload_preferences()
        if default_file:
            self.open_form(default_file)
        else:
            self.change_page(Factory.Note())
        self.search_bar.results.dismiss()

    def _close_keyboard(self):
        self.keyboard.unbind(on_key_down=self.on_key_down)
        self.keyboard = None

    def get_search_results(self, text):
        for path, form in self.raw_data.items():
            frm = r'(.{{0,10}})({})(.{{0,10}})'.format(text)
            result = re.search(frm, form)
            if result:
                yield (path, result.group(1), result.group(2), result.group(3))

    def on_input(self, widget, text):
        ddn = self.search_bar.results

        ddn.clear_widgets()
        if len(text) < len(self.last_search_text) and \
                text != self.last_search_text:
            self.reload_raw_data()
        if len(text) == 0:
            ddn.dismiss()
            return
        self.last_search_text = text
        results = self.get_search_results(text)
        for path, pre, match, post in results:
            text = ''.join((
                '{path}\n'.format(path=path),
                '[color=#C6C6C6]...{pre}[/color]'.format(pre=pre),
                '[b]{match}[/b]'.format(match=match),
                '[color=#C6C6C6]{post}...[/color]'.format(post=post),
            ))
            btn = Button(
                text=text,
                size_hint=(1, None),
                height=35,
                markup=True,
            )
            btn.bind(on_release=lambda w, path=path: ddn.select(path))
            ddn.add_widget(btn)
        ddn.open(self.search_bar.entry)

    def change_page(self, widget):
        def _on_form_change(*args):
            self.current_form.changed = True
        lyt = self.lyt_content
        lyt.clear_widgets()
        lyt.add_widget(widget)
        self.current_form = widget
        self.current_form.changed = False
        self.current_form.save.bind(
            on_release=lambda w: self.save_form_data()
        )
        for key, attr in self.current_form.values.items():
            try:
                field = getattr(self.current_form, key)
            except AttributeError:
                continue
            field.bind(**{attr: _on_form_change})

    def on_key_down(self, keyboard, key, scancode, text, modifiers):
        if scancode == 22:  # 's' on a QWERTZ keyboard
            if 'ctrl' in modifiers:
                self.save_form_data()
        elif scancode == 59:  # F2
            self.search_bar.entry.focus = True
        # print(key, scancode, modifiers)

    def open_form(self, path):
        self.search_bar.results.dismiss()
        try:
            raw = self.raw_data[path]
        except KeyError:
            with open(path, 'r') as f:
                raw = f.read()
            self.raw_data[path] = raw
        data = yaml.load(raw)
        Form = getattr(Factory, data['type'])
        form = Form()
        form.path.text = path
        for key, attr in form.values.items():
            try:
                value = data['data'][key]
                field = getattr(form, key)
            except (AttributeError, KeyError):
                continue
            setattr(field, attr, value)
        self.change_page(form)

    def reload_preferences(self):
        preferences = self.raw_data['data\\preferences.bd']
        self.preferences = yaml.load(preferences)['data']

    def reload_raw_data(self):
        self.raw_data = {}
        paths = self.preferences['search_paths']
        for path in paths:
            for name in os.listdir(path):
                full_path = os.path.join(path, name)
                with open(full_path, 'r') as f:
                    content = f.read()
                self.raw_data[full_path] = content

    def save_form_data(self):
        form = self.current_form
        data = {}
        for key, attr in form.values.items():
            try:
                field = getattr(form, key)
            except (AttributeError, KeyError):
                continue
            value = getattr(field, attr)
            data[key] = value
        old = yaml.load(self.raw_data[form.path.text])
        old['data'] = data
        with open(form.path.text, 'w') as f:
            f.write(yaml.dump(old, default_flow_style=False))
        form.save.disabled = True
