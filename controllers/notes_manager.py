

import os
import re

import yaml


class NotesManager:
    def __init__(self):
        self.notes = {}
        self.refresh_notes()
        self.current_note = None

    def refresh_notes(self):
        self.notes.clear()
        preferences = self.load_note('data/preferences.bds')
        data = preferences['widgets'][0]['data']
        preferences = self.load_note('data/user_preferences.bds')
        if preferences:
            data.update(['widgets'][0]['data'])
        for path in data['search_paths']:
            for item in os.listdir(path):
                if not item.endswith('.bd'):
                    continue
                complete_path = '/'.join((path, item))
                note = self.load_note(complete_path)
                self.notes[complete_path] = note

    def search_notes(self, expr, in_title=True):
        for path, note in sorted(self.notes.items(), reverse=True):
            title = note['title']
            checks = []
            if in_title:
                checks.append(title)

            for check in checks:
                match = re.search(expr, check)
                if match:
                    span = match.span()
                    hint_start = max(span[0] - 4, 0)
                    hint_end = min(span[1] + 4, len(check))
                    yield path, ''.join((
                        '...',
                        check[hint_start:hint_end],
                        '...',
                    ))

    def load_note(self, path):
        with open(path, 'r') as f:
            content = f.read()
        return yaml.load(content)

    def save_note(self, path, content):
        dump = yaml.dump(content, default_flow_style=False)
        with open(path, 'w') as f:
            f.write(dump)
