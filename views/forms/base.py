

from kivy.uix.boxlayout import BoxLayout


class Base(BoxLayout):
    form_type = 'base'

    def __init__(self, data, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.create(data)

    def create(self, data):
        return

    def save(self):
        return
