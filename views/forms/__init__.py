

import os
from importlib import import_module


FORMS = {}
for filename in os.listdir(os.path.dirname(__file__)):
    if filename.endswith('.py') and not filename.startswith('__'):
        modname = filename[:-3]
        FORMS[modname] = getattr(
            import_module('.'.join(['views', 'forms', modname])),
            modname.title().replace('_', ''),
        )
