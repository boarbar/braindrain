

from kivy.lang import Builder

from .base import Base


Builder.load_file('views/forms/note.kv')


class Note(Base):
    form_type = 'note'

    def create(self, data):
        self.content.text = data

    def save(self):
        return self.content.text
