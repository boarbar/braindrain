

from kivy.lang import Builder
from kivy.uix.togglebutton import ToggleButton

from views.base_screen import BaseScreen


Builder.load_file('views/search_screen.kv')


class SearchScreen(BaseScreen):
    selected_result = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'up':
            if not self.selected_result:
                # self.results.children[0].dispatch('on_touch_down', None)
                wg = self.results.children[0]
                wg.state = 'down'
                self.selected_result = wg
            else:
                for i, child in enumerate(self.results.children[:-1]):
                    if child.state == 'down':
                        child.state = 'normal'
                        wg = self.results.children[i + 1]
                        wg.state = 'down'
                        self.selected_result = wg
                        break
                else:
                    self.results.children[i + 1].state = 'normal'
                    self.selected_result = None
                    self.entry.focus = True
        elif keycode[1] == 'down':
            if not self.selected_result:
                wg = self.results.children[-1]
                wg.state = 'down'
                self.selected_result = wg
            else:
                for i, child in enumerate(reversed(
                        self.results.children[1:]), 2):
                    if child.state == 'down':
                        child.state = 'normal'
                        wg = self.results.children[-i]
                        wg.state = 'down'
                        self.selected_result = wg
                        break
                else:
                    self.results.children[0].state = 'normal'
                    self.selected_result = None
                    self.entry.focus = True
        elif keycode[1] == 'enter':
            if self.selected_result:
                path = self.selected_result.path
                self.parent.notes_manager.current_note = path
                self.parent.transition.direction = 'up'
                self.parent.current = 'note'
        else:
            super()._on_keyboard_down(keyboard, keycode, text, modifiers)

    def update_results(self):
        self.results.clear_widgets()
        matches = self.parent.notes_manager.search_notes(self.entry.text)
        for path, match in matches:
            button = ToggleButton(
                text='{}\n[b]{}[/b]'.format(path, match),
                markup=True,
                group='results',
                height=30,
                size_hint_y=None,
                # pos_hint={'top': 1},
            )
            button.path = path
            button.bind(on_touch_down=self.select_result)
            self.results.add_widget(button)
        self.results.children.reverse()

    def select_result(self, wg, ev):
        super(ToggleButton, wg).on_touch_down(ev)
        self.selected_result = wg
