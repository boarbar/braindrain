

from kivy.uix.screenmanager import Screen


class BaseScreen(Screen):
    def __init__(self, previous=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.previous = previous

    def on_parent(self, child, parent):
        if parent:
            parent.keyboard.bind(on_key_down=self._on_keyboard_down)

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'escape' and self.previous:
            self.parent.transition.direction = 'down'
            self.parent.current = self.previous
        return
