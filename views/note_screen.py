

from kivy.lang import Builder

from views.base_screen import BaseScreen
from views.forms import FORMS


Builder.load_file('views/note_screen.kv')


class NoteScreen(BaseScreen):
    def on_parent(self, instance, parent):
        super().on_parent(instance, parent)
        if parent:
            self.save.bind(on_press=self.save_current)
            current = parent.notes_manager.current_note
            content = parent.notes_manager.load_note(current)
            self.path.text = current
            self.title.text = content['title']
            for widget in content['widgets']:
                form = FORMS[widget['type']](widget['data'])
                self.content.add_widget(form)
        else:
            self.save.unbind(on_press=self.save_current)
            self.path.text = ''
            self.title.text = ''
            self.content.clear_widgets()

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'enter':
            print(self.parent.notes_manager.current_note)
        else:
            super()._on_keyboard_down(keyboard, keycode, text, modifiers)

    def save_current(self, wg):
        title = self.title.text
        path = self.path.text
        widgets = []
        for widget in self.content.children:
            content = widget.save()
            widgets.append({
                'type': widget.form_type,
                'data': content,
            })
        self.parent.notes_manager.save_note(
            path,
            {
                'title': title,
                'widgets': widgets,
                'tags': []  # tags,
            },
        )
