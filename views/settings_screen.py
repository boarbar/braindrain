

from kivy.lang import Builder

from views.base_screen import BaseScreen


Builder.load_file('views/settings_screen.kv')


class SettingsScreen(BaseScreen):
    pass
